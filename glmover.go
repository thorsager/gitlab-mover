package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab-mover/gitlab"
	"gopkg.in/alecthomas/kingpin.v2"
	"io/ioutil"
	"log"
	"os"
	"path"
	"regexp"
	"time"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("error loading .env file")
	}

	var app = kingpin.New("glmover", "A commandline tool for migrating gitlab projects")
	//var appDebug = app.Flag("debug","Enable debug output").Bool()
	var appToken = app.Flag("token", "Auth token, to be used for server auth (env: GITLAB_TOKEN)").
		Default(os.Getenv("GITLAB_TOKEN")).String()
	var appUrl = app.Flag("url", "Gitlab Server Url (env: GITLAB_URL)").
		Default(os.Getenv("GITLAB_URL")).String()

	var impCmd = app.Command("import", "Import projects from files to server")
	var impSrc = impCmd.Flag("src", "Import source file or folder (env: DOWNLOAD_FOLDER)").
		Default(os.Getenv("DOWNLOAD_FOLDER")).String()
	var impPrefix = impCmd.Flag("prefix", "Namespace prefix for all imported projcts").String()

	var expCmd = app.Command("export", "export project from server to files")
	var expDst = expCmd.Flag("dst", "Export destination folder (env: DOWN_FOLDER)").
		Default(os.Getenv("DOWNLOAD_FOLDER")).String()

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case impCmd.FullCommand():
		err := importProjects(gitlab.New(*appUrl).SetAuthToken(*appToken), impSrc, impPrefix)
		if err != nil {
			log.Fatal(err)
		}
	case expCmd.FullCommand():
		err := exportProjects(gitlab.New(*appUrl).SetAuthToken(*appToken), expDst)
		if err != nil {
			log.Fatal(err)
		}
	}
}

// exportProject exports all projects from a gitlab-host.
// The exported projects are placed in the passed "dst" folder
func exportProjects(c *gitlab.ApiClient, dst *string) error {
	projects, err := c.GetProjects()
	if err != nil {
		return nil
	}
	for idx, p := range *projects {
		log.Printf("- %d: %+v", idx, p)

		err := c.ExportProject(p.Id)
		if err != nil {
			return err
		}

		for true {
			s, err := c.ExportStatus(p.Id)
			if err != nil {
				log.Fatal(err)
			}
			if s.Status == "finished" {
				log.Println("Export done!")
				_, err := c.ExportDownload(p.Id, *dst)
				if err != nil {
					return err
				}
				break
			}
			log.Printf("Status: %s", s.Status)
			time.Sleep(time.Duration(500 * time.Millisecond))
		}
	}
	return nil
}

func importProjects(c *gitlab.ApiClient, src *string, prefix *string) error {
	info, err := os.Stat(*src)
	if err != nil {
		return nil
	}

	if info.IsDir() {
		files, err := ioutil.ReadDir(*src)
		if err != nil {
			return nil
		}
		for _, file := range files {
			fStat, err := os.Stat(path.Join(*src, file.Name()))
			if fStat.IsDir() {
				continue
			}
			pif, err := parseFileName(path.Join(*src, file.Name()), *prefix)
			if err != nil {
				log.Printf("%s: %s", file.Name(), err.Error())
			}
			fmt.Printf("%+v\n", pif)
			_, err = c.ImportProject(pif.Filename, pif.asImportOpts())
			if err != nil {
				log.Fatalf("import faild: %+v", err)
			}

			done := path.Join(*src, ".done")
			if _, err := os.Stat(done); os.IsNotExist(err) {
				err := os.MkdirAll(done, 0755)
				if err != nil {
					return err
				}
			}

			err = os.Rename(path.Join(*src, file.Name()), path.Join(done, file.Name()))
			if err != nil {
				return err
			} else {
				log.Printf("moved %s", file.Name())
			}
		}
	} else {
		pif, err := parseFileName(*src, *prefix)
		if err != nil {
			log.Fatalf("%s: %s", *src, err.Error())
		}
		fmt.Printf("%+v\n", pif)
		_, err = c.ImportProject(pif.Filename, pif.asImportOpts())
		if err != nil {
			log.Fatalf("import faild: %+v", err)
		}
	}
	return nil
}

type ProjectFileInfo struct {
	Filename  string
	TimeStr   string
	Namespace string
	Name      string
}

func (p *ProjectFileInfo) asImportOpts() *gitlab.ImportOptions {
	return &gitlab.ImportOptions{Namespace: p.Namespace, Path: p.Name, Overwrite: true}
}

func parseFileName(filename string, namespacePrefix string) (*ProjectFileInfo, error) {
	exp, err := regexp.Compile(`^(\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{3})_([a-zA-Z0-9\-]*)_(.*)_export\.tar\.gz$`)
	if err != nil {
		log.Fatalf("%+v", err)
	}
	match := exp.FindStringSubmatch(path.Base(filename))
	if len(match) != 4 {
		return nil, fmt.Errorf("unable to match '%s'", filename)
	}
	namespace := match[2]
	if namespacePrefix != "" {
		namespace = namespacePrefix + "/" + namespace
	}
	return &ProjectFileInfo{TimeStr: match[1], Namespace: namespace, Name: match[3], Filename: filename}, nil
}
