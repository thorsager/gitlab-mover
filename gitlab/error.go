package gitlab

import (
	"io/ioutil"
	"net/http"
)

type ClientError struct {
	message  string
	response *http.Response
}

func (ce *ClientError) Error() string {
	msg := ce.message
	if ce.response.Status != "" {
		msg += "\n" + ce.response.Status + "\n\n"
	}
	defer ce.response.Body.Close()
	body, err := ioutil.ReadAll(ce.response.Body)
	if err != nil {
		msg += err.Error()
	} else {
		if len(body) != 0 {
			msg += string(body)
		}
	}
	return msg
}

func newError(message string, response *http.Response) *ClientError {
	return &ClientError{message: message, response: response}
}
