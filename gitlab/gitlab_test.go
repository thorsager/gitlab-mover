package gitlab

import (
	"os"
	"testing"
)

func TestApiClient_Login(t *testing.T) {
	url := os.Getenv("T_GL_URL")
	user := os.Getenv("T_GL_USER")
	password := os.Getenv("T_GL_PASSWORD")
	if url != "" && user != "" && password != "" {
		c := New(url)
		err := c.Login(user, password)
		if err != nil {
			t.Fatal(err)
		}
	} else {
		t.Log("Ignored, set T_GL_ULR, T_GL_USER and T_GL_PASSWORD")
	}
}

func TestApiClient_GetGroups(t *testing.T) {
	url := os.Getenv("T_GL_URL")
	token := os.Getenv("T_GL_TOKEN")
	if url != "" && token != "" {
		c := New(url).SetAuthToken(token)
		groups, err := c.GetGroups()
		if err != nil {
			t.Fatal(err)
		}
		if groups == nil || len(*groups) == 0 {
			t.Fatal("no Groups?")
		} else {
			t.Logf("%+v", groups)
		}
	} else {
		t.Log("Ignored, set T_GL_URL and T_GL_TOKEN")
	}
}

func TestApiClient_SearchGroups(t *testing.T) {
	url := os.Getenv("T_GL_URL")
	token := os.Getenv("T_GL_TOKEN")
	if url != "" && token != "" {
		c := New(url).SetAuthToken(token)
		groups, err := c.SearchGroups("thorsager-legacy")
		if err != nil {
			t.Fatal(err)
		}
		if groups == nil || len(*groups) == 0 {
			t.Fatal("no Groups?")
		} else {
			t.Logf("%+v", groups)
		}
	} else {
		t.Log("Ignored, set T_GL_URL and T_GL_TOKEN")
	}
}
