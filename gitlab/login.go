package gitlab

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func (c *ApiClient) Login(user, password string) error {
	loginUrl := c.url + "/users/sign_in"
	log.Printf("Loggin in (%s)\n", loginUrl)

	if c.httpClient == nil {
		c.httpClient = defaultClient()
	}
	aToken, err := getAuthenticityToken(c.httpClient, loginUrl)
	if err != nil {
		return err
	}

	fmt.Printf("Authentisity Token = %s\n", aToken)

	form := &url.Values{}
	form.Add("authenticity_token", aToken)
	form.Add("user[login]", user)
	form.Add("user[password]", password)
	form.Add("user[remember_me]", "1")

	req, err := http.NewRequest(http.MethodPost, loginUrl, strings.NewReader(form.Encode()))
	if err != nil {
		return err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 302 { // redirects on successful login
		log.Printf("HEADER:\n%+v", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)
		log.Printf("BODY:\n%s", body)
		return fmt.Errorf("login failed: %d - %s", resp.StatusCode, resp.Status)
	}
	fmt.Printf("%+v\n", c.httpClient.Jar)
	return nil
}

func getAuthenticityToken(client *http.Client, url string) (string, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return "", err
	}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("status %d, %s", resp.StatusCode, resp.Status)
	}

	aToken, err := authenticityTokenScraper(resp.Body)
	if err != nil {
		return "", err
	}
	return aToken, nil
}

func authenticityTokenScraper(reader io.Reader) (string, error) {
	var token string
	var found bool
	doc, err := goquery.NewDocumentFromReader(reader)
	if err != nil {
		return "", err
	}
	doc.Find("form input[name=authenticity_token]").Each(func(i int, selection *goquery.Selection) {
		token, found = selection.Attr("value")
	})
	if !found || token == "" {
		return "", fmt.Errorf("authenticity_token not found")
	}
	return token, nil
}
