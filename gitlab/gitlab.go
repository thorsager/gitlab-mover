package gitlab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"mime/multipart"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
)

const perPage = "10000"

type ApiClient struct {
	url        string
	token      string
	httpClient *http.Client
	logger     *log.Logger
}

type Project struct {
	Id        int       `json:"id"`
	Name      string    `json:"name"`
	Namespace Namespace `json:"namespace"`
}

type Namespace struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Path     string `json:"path"`
	Kind     string `json:"kind"`
	FullPath string `json:"full_path"`
}

type Group struct {
	Id          int    `json:"id"`
	ParentId    int    `json:"parent_id"`
	Name        string `json:"name"`
	FullName    string `json:"full_name"`
	Path        string `json:"path"`
	FullPath    string `json:"full_path"`
	Description string `json:"description"`
	Visibility  string `json:"visibility"`
	LfsEnabled  bool   `json:"lfs_enabled"`
	AvatarUrl   string `json:"avatar_url"`
	WebUrl      string `json:"web_url"`
}

type ExportStatus struct {
	Id                int    `json:"id"`
	Status            string `json:"export_status"`
	Path              string `json:"path"`
	PathWithNamespace string `json:"path_with_namespace"`
}

type ImportStatus struct {
	Id                int    `json:"id"`
	Name              string `json:"name"`
	PathWithNamespace string `json:"path_with_namespace"`
	Status            string `json:"import_status"`
}

type ImportOptions struct {
	Namespace string
	Path      string
	Overwrite bool
}

func New(baseUrl string) *ApiClient {
	return &ApiClient{url: baseUrl}
}

func (c *ApiClient) SetAuthToken(token string) *ApiClient {
	c.token = token
	return c
}

func defaultClient() *http.Client {
	cookieJar, _ := cookiejar.New(nil)
	return &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Jar: cookieJar,
	}
}

func defaultLogger() *log.Logger {
	return log.New(os.Stderr, "gitlab-api-client: ", log.Lshortfile|log.LstdFlags)
}

func (c *ApiClient) SearchGroups(nameOrPath string) (*[]Group, error) {
	apiUrl, err := c.urlWithPath("/groups")
	if err != nil {
		return nil, err
	}
	q := apiUrl.Query()
	q.Set("search", nameOrPath)
	apiUrl.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, apiUrl.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.doRequest(req, 200)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	groups := make([]Group, 0)
	err = json.Unmarshal(body, &groups)
	if err != nil {
		return nil, err
	}
	return &groups, nil

}

func (c *ApiClient) GetGroups() (*[]Group, error) {
	apiUrl, err := c.urlWithPath("/groups")
	if err != nil {
		return nil, err
	}
	q := apiUrl.Query()
	apiUrl.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, apiUrl.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.doRequest(req, 200)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	groups := make([]Group, 0)
	err = json.Unmarshal(body, &groups)
	if err != nil {
		return nil, err
	}
	return &groups, nil
}

func (c *ApiClient) GetProjects() (*[]Project, error) {
	apiUrl, err := c.urlWithPath("/projects")
	if err != nil {
		return nil, err
	}
	q := apiUrl.Query()
	q.Set("per_page", perPage)
	apiUrl.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, apiUrl.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.doRequest(req, 200)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	projects := make([]Project, 0)
	err = json.Unmarshal(body, &projects)
	if err != nil {
		return nil, err
	}
	return &projects, nil
}

func (c *ApiClient) urlWithPath(pathFormat string, a ...interface{}) (*url.URL, error) {

	u, err := url.Parse(c.url)
	if err != nil {
		return nil, err
	}
	u.Path = fmt.Sprintf("/api/v4"+pathFormat, a...)
	return u, nil
}

func (c *ApiClient) ExportProject(projectId int) error {
	apiUrl, err := c.urlWithPath("/projects/%d/export", projectId)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, apiUrl.String(), nil)
	if err != nil {
		return err
	}

	_, err = c.doRequest(req, 202)
	if err != nil {
		return err
	}
	return nil
}

func (c *ApiClient) ExportStatus(projectId int) (*ExportStatus, error) {
	apiUrl, err := c.urlWithPath("/projects/%d/export", projectId)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, apiUrl.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.doRequest(req, 200)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	status := &ExportStatus{}
	err = json.Unmarshal(body, status)
	if err != nil {
		return nil, err
	}
	return status, nil
}

func (c *ApiClient) ExportDownload(projectId int, destFolder string) (string, error) {
	apiUrl, err := c.urlWithPath("/projects/%d/export/download", projectId)
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest(http.MethodGet, apiUrl.String(), nil)
	if err != nil {
		return "", err
	}

	resp, err := c.doRequest(req, 200)
	if err != nil {
		return "", err
	}

	disposition, params, err := mime.ParseMediaType(resp.Header.Get("Content-Disposition"))
	if err != nil {
		return "", err
	}

	if _, err := os.Stat(destFolder); os.IsNotExist(err) {
		os.MkdirAll(destFolder, 0755)
	}

	dst, err := os.Create(filepath.Join(destFolder, params["filename"]))
	if err != nil {
		return "", err
	}
	defer dst.Close()

	written, err := io.Copy(dst, resp.Body)
	log.Printf("Downloaded %d bytes to from: %s; %s", written, disposition, params["filename"])

	return dst.Name(), nil
}

func (c *ApiClient) ImportProject(filename string, opts *ImportOptions) (*ImportStatus, error) {
	apiUrl, err := c.urlWithPath("/projects/import")
	log.Printf("URL: %s", apiUrl)

	bodyBuffer := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuffer)

	// add file to body
	fileWriter, err := bodyWriter.CreateFormFile("file", filename)
	if err != nil {
		return nil, err
	}

	fh, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer fh.Close()

	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return nil, err
	}

	if opts.Namespace != "" {
		namespaceWriter, err := bodyWriter.CreateFormField("namespace")
		if err != nil {
			return nil, err
		}
		_, err = namespaceWriter.Write([]byte(opts.Namespace))
		if err != nil {
			return nil, err
		}
	}

	if opts.Path != "" {
		pathWriter, err := bodyWriter.CreateFormField("path")
		if err != nil {
			return nil, err
		}
		_, err = pathWriter.Write([]byte(opts.Path))
		if err != nil {
			return nil, err
		}
	}

	if opts.Overwrite {
		overwriteWriter, err := bodyWriter.CreateFormField("overwrite")
		if err != nil {
			return nil, err
		}
		_, err = overwriteWriter.Write([]byte("true"))
		if err != nil {
			return nil, err
		}
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	req, err := http.NewRequest(http.MethodPost, apiUrl.String(), bodyBuffer)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-type", contentType)
	resp, err := c.doRequest(req, 201)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	status := &ImportStatus{}
	log.Printf("%s\n", string(body))
	err = json.Unmarshal(body, status)
	if err != nil {
		return nil, err
	}
	return status, nil

}

func (c *ApiClient) ImportStatus(projectId int) (*ImportStatus, error) {
	apiUrl, err := c.urlWithPath("/projects/%d/import", projectId)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, apiUrl.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	status := &ImportStatus{}
	err = json.Unmarshal(body, status)
	if err != nil {
		return nil, err
	}
	return status, nil
}

func (c *ApiClient) doRequest(req *http.Request, expect ...int) (*http.Response, error) {
	if c.httpClient == nil {
		c.httpClient = defaultClient()
	}
	req.Header.Add("PRIVATE-TOKEN", c.token)
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	if !foundIn(resp.StatusCode, expect) {
		return nil, newError(fmt.Sprintf("\n%s %s", req.Method, req.URL.String()), resp)
	}
	return resp, nil
}

func foundIn(needle int, hayStack []int) bool {
	for _, v := range hayStack {
		if v == needle {
			return true
		}
	}
	return false
}

func findGroupByName(name string, groups *[]Group) (*Group, error) {
	for _, group := range *groups {
		if group.Name == name {
			return &group, nil
		}
	}
	return nil, fmt.Errorf(fmt.Sprintf("group %s not found", name))
}

func (c *ApiClient) log(v ...interface{}) {
	if c.logger == nil {
		c.logger = defaultLogger()
	}
	c.logger.Print(v...)
}

func (c *ApiClient) debugf(format string, v ...interface{}) {
	if c.logger == nil {
		c.logger = defaultLogger()
	}
	c.logger.Printf(format, v...)
}
